https://hedgedoc.obermui.de/fdMH7q68TWyn2fBBHzevlA?both

---
title: Woodpecker CI - A Brief Introduction
type: slide
slideOptions:
  transition: slide
  theme: white
---
<!-- .slide: data-background="https://cloud.obermui.de/s/2apJSkbYSdTgqgJ/preview" -->

# Woodpecker CI - A Brief Introduction
![](https://raw.githubusercontent.com/woodpecker-ci/woodpecker/master/web/src/assets/woodpecker.svg) <!-- .element: style="border: none; width: 10%" -->

note: welcome the audience

---

<!-- .slide: data-background="https://cloud.obermui.de/s/2apJSkbYSdTgqgJ/preview" -->
# ~> whoami

![](https://codeberg.org/avatars/09a234c768cb9bca78f6b2f82d6af173)  <!-- .element: style="border: none; width: 10%" -->

---
codeberg.org/6543 - github.com/6543 - @6543@chaos.social

note: this presentation only presents my personal opinions

---

# Structur

## - CI/CD ?!?
## - Project Overview
## - Concepts
## - Applications
## - Live Demo(s)

note: who is it for and whats the aim

---

# CI/CD

----


![](https://freepngimg.com/download/compact%20disc/12-compact-cd-dvd-disk-png-image.png)  <!-- .element: style="border: none; width: 50%" -->

note: joke about CDs

----

![](https://www.synopsys.com/content/dam/synopsys/sig-assets/images/cicd-loop.jpg.imgw.850.x.jpg)

note: 

---

# Project Overview

----

## Lightweight
## Easy to selfhost
## Easy to extend
## Apache 2 License
## Security in mind

Note:
Has it's roots in DroneCI
easy because of simplicity

----

## Integrate with:

![Gitlab](https://cdn.freebiesupply.com/logos/large/2x/gitlab-logo-png-transparent.png)  <!-- .element: style="border: none; width: 10%" -->
![Github](https://pngimg.com/uploads/github/github_PNG40.png)  <!-- .element: style="border: none; width: 10%" -->
![Gitea](https://docs.gitea.io/images/gitea.png)  <!-- .element: style="border: none; width: 10%" -->

note:
 - nice if you have multible forges as only one syntax is required
 - bitbucked kinda
 - new easy to add
 - pure yaml spec

---

# Concepts

----

## Software Componentes

### Server
### Agent
### CLI

note:
 tldr: what do we need to run it
 mention CLI as terminal tool for debugging and linting

----

## Configuration

### Alles in YAML Files im repo
### Außer Secrets .... 

----

## Pipeline

- are triggered by **events**
- One or more sequence of **workflows**
- **workflows** can depend on each other

----

## Workflow

- contains one or more **steps**
- has it's own **isolated** environment
- can contain **services**
- are just **YAML** files

note:
- environment = own network & docker volume

----

## Steps

- execs individual commands **or**
- predefine commands called **plugins**
- are just **container**

----

## Services

- have a defined network address
- starts at **workflow** beginning and end with it
- are just detatched **steps**

----

## Plugins

- make woodpecker **extensible**
- slim down big **commands** into **settings**
- are more **secure**

---
*https://woodpecker-ci.org/plugins*

note:
- slim ? why?
- why more secure?
- TODO: show plugin index?

---

# Applications

----

## Automate tasks around Development

 Testing - Releasing - Maintaining
 
note:
 just chit-chat ... there are enouth things 
 - yes Anisble Sempahore was replaced by it too for someone

---

# Live Demo(s)
## - setup a local instance
## - compile a golang project
## - create a simple plugin
## - publish the go project

---

# Questions